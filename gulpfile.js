'use strict';

const gulp = require('gulp'),
	rename = require('gulp-rename'),
	concat = require('gulp-concat'),
	uglify = require('gulp-uglify'),
	git = require('gulp-git');

module.exports = {
	default: dist,
	dist,
};

function dist() {
	return gulp
		.src(['app.js', 'lib/*'])
		.pipe(concat('sp-api.js'))
		.pipe(gulp.dest('dist'))
		.pipe(git.add())
		.pipe(uglify({
			compress: true
		}))
		.pipe(rename('sp-api.min.js'))
		.pipe(gulp.dest('dist'))
		.pipe(git.add());
}