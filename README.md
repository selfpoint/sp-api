#sp-api
api service for angularjs

##parameters
###ApiProvider
* `setParameters` optional
* `addRetry(name, options)` - options are: `{status: Number|Array, callback: Function|Array, limit: [Number]}`
* `httpMethodOverride` optional
###LoadingProvider
* `loadingTemplate` required
* `showLoadingAfter` optional
* `hideLoadingAfter` optional

##Events
* `spApi.error` - called when got error and the request is not flagged as `fireAndForgot`
* `spApi.response` - called when got response (either error or success)

##commit
before any commit do `gulp` (to create dist file)