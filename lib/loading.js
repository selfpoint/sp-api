;
(function(angular) {
    'use strict';

    angular.module('spApi').config(['$provide', function ($provide) {
        var provider = $provide.service('Loading', ['$q', '$location', '$rootScope', '$compile', function ($q, $location, $rootScope, $compile) {
            var self = this;

            /**
             * @private
             * counter per loading element and location
             *
             * @example
             * {
             *   'sp-api-1': {
             *     '/path': 2,
             *     '/otherPath': 1
             *   },
             *   'sp-api-2': {
             *     '/path': 1
             *   }
             * }
             */
            self._requestCounter = {};

            /**
             * set counter by element and path
             *
             * @param {HTMLElement} element
             * @param {Number} toAdd
             * @param {Object} options
             * @param {String} [options.path=$location.path() || '/']
             * @param {String} [options.activeClass]
             *
             * @return {String} path
             */
            self.counter = function (element, toAdd, options) {
                options = options || {};
                var isDefaultElement = !element;
                element = self._getElement(element);

                var id = self._idByElement(element);

                var path = (typeof options.path === 'string' ? options.path :  $location.path() || '/');

                if (!self._requestCounter[id]) {
                    self._requestCounter[id] = {};
                }

                if (!self._requestCounter[id][path]) {
                    self._requestCounter[id][path] = toAdd;
                } else {
                    self._requestCounter[id][path] += toAdd;
                }

                if (self._requestCounter[id][path]) {
                    setTimeout(function() {
                        if (!self._requestCounter[id][path]) return;

                        _toggleElement(element, isDefaultElement, true, options);
                    }, provider.showLoadingAfter);
                } else {
                    setTimeout(function() {
                        if (self._requestCounter[id][path]) return;

                        _toggleElement(element, isDefaultElement, false, options);
                    }, provider.hideLoadingAfter);

                }

                return path;
            };

            /**
             * Toggle loading element on or off
             * @private
             *
             * @param {Element} element
             * @param {boolean} isDefaultElement
             * @param {boolean} isOn
             * @param {Object} options
             * @param {string} [options.activeClass]
             *
             * @returns {void}
             */
            function _toggleElement(element, isDefaultElement, isOn, options) {
                var className = isDefaultElement ? provider.activeClass : options.activeClass;
                if (className) {
                    isOn ? element.addClass(className) : element.removeClass(className);
                } else {
                    element.css('display', isOn ? 'block' : 'none');
                }

                if (isDefaultElement) {
                    var body = angular.element(document.body || document.querySelector('body'));
                    isOn ? body.addClass('has-loading') : body.removeClass('has-loading');
                }
            }

            /**
             * set count by element and path
             *
             * @param {Element} element
             * @param {String=$location.path()} [path]
             *
             * @return {Number} count
             */
            self.getCount = function (element, path) {
                element = self._getElement(element);

                var id = self._idByElement(element);

                path = path || $location.path();

                var count = 0;
                if (self._requestCounter[id] && self._requestCounter[id][path]) {
                    count = self._requestCounter[id][path];
                }
                return count;
            };

            /**
             * @private
             * get element or default elem,element
             *
             * @return {Element}
             */
            self._getElement = function (element) {
                return (element ? angular.element(element) : null) || self._getDefaultElement();
            };

            /**
             * angular element of _defaultElement (created on the first time we need this)
             * @type {HTMLElement}
             * @private
             */
            self._defaultElement = null;

            /**
             * @private
             * return (create if need) default element
             *
             * @return {Element}
             */
            self._getDefaultElement = function () {
                if (self._defaultElement) {
                    return self._defaultElement;
                }

                self._defaultElement = angular.element(provider.loadingTemplate);
                _toggleElement(self._defaultElement, true, false, {});
                angular.element(document.body).append(self._defaultElement);
                $compile(self._defaultElement)($rootScope);

                return self._defaultElement;
            };

            /**
             * @private
             * number for _generateId
             * @type {Number}
             */
            self._generateIdNumber = 1;

            /**
             * @private
             * return unique id ('sp-api-{{Number}}')
             *
             * @return {String} id
             */
            self._generateId = function () {
                return 'sp-api-' + (self._generateIdNumber++);
            };

            /**
             * @private
             * get id (sp-api id) by element
             *
             * @param {Element} element
             *
             * @return {String} id
             */
            self._idByElement = function (element) {
                var spApiId = element.prop('sp-api-id');
                if (spApiId) {
                    return spApiId;
                }

                var id = self._generateId();
                element.prop('sp-api-id', id);
                return id;
            };
        }]);

        /**
         * default loading html template
         * @type {String}
         */
        provider.loadingTemplate = new Error('Loading.loadingTemplate must overwrite');

        /**
         * default active class
         * @type {string}
         */
        provider.activeClass = null;

        /**
         * show loading element after X ms
         * @type {Number}
         */
        provider.showLoadingAfter = null;

        /**
         * show loading element after X ms
         * @type {Number}
         */
        provider.hideLoadingAfter = null;
    }]);
})(angular);
